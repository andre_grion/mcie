## 1) Sejam Y_1, ..., Y_n v.a. independentes com distribuição Burr Type III de parâmetros c = 1 e d desconhecido.
## A fdp é dado por:
## f(y; c,d) = cdy^(-c-1)(1+y^(-c))^(-d-1) para y > 0 e c,d > 0
## Considere que uma amostra y_1, ..., y_n desta v.a. será disponível.

## Densidade genérica
##dburr3 <- function(y, c, d) c*d*y^(-c-1)*(1+y^(-c))^(-d-1)

## Densidade p/ c = 1
dburr3 <- function(y, d) d*y^(-2)*(1+y^(-1))^(-d-1)

## É uma fdp:
##curve(dburr3(y, d = 3), 0.1, 20, xname = "y", add = TRUE)
##integrate(dburr3, 0, Inf, d = 3)

### a) Escreva a função de verossimilhança, log-verossimilhança, verossimilhança relativa e deviance

#### verossimilhança
L <- function(d, y){
    n <- length(y)
    return(d^n*prod(y^(-2))*prod(1+y^(-1))^(-d-1))
}

curve(L(d, c(3, 2, 5:7)), from = 0.1, to = 20, xname = "d")

#### log-verossimilhança
l <- function(d, y){
    n <- length(y)
    n*log(d) + log(prod(y^(-2))) - (d+1)*log(prod(1+y^(-1)))
}

curve(l(d, c(3, 2, 5:7)), from = 0.1, to = 20, xname = "d", lwd = 1)

l2 <- function(d, y){
    n <- length(y)
    n*log(d) -2*sum(log(y)) - (d+1)*sum(log(1+y^(-1)))
}

curve(l2(d, c(3, 2, 5:7)), from = 0.1, to = 20, xname = "d", add = TRUE, col = 2, lty = 2, lwd = 3)

#### verossimilhança relativa
RL <- function(d, d0, y){
    n <- length(y)
    (d^n*prod(1+y^(-1))^(-d-1))/(d0^n*prod(1+y^(-1))^(-d0-1))
}

curve(RL(d, d0 = 4.29, y = c(3, 2, 5:7)), from = 0.1, to = 20, xname = "d", lwd = 1)
abline(h = 1, col = 2)

RL2 <- function(d, d0, y){
    n  <- length(y)
    (d/d0)^n*prod(1+y^(-1))^(d0-d)
}

curve(RL2(d, d0 = 4.29, y = c(3, 2, 5:7)), from = 0.1, to = 20, xname = "d", add = TRUE, col = 2, lty = 2, lwd = 3)

#### deviance
Dev0 <- function(d, d0, y){
    -2*(l(d, y)-l(d0, y))
}

curve(Dev0(d, d0 = 4.29, y = c(3, 2, 5:7)), from = 0.1, to = 10, xname = "d")

##curve(Dev0(d, d0 = 4.29, y = c(3, 2, 5:7)), from = 1.5, to = 9.6, xname = "d")

Dev <- function(d, d0, y){
    n <- length(y)
    -2*(n*(log(d) - log(d0)) + sum(log(1+y^(-1)))*(d0 - d))
}

curve(Dev(d, d0 = 4.29, y = c(3, 2, 5:7)), from = 0.1, to = 10, xname = "d", add = TRUE, col = 3, lty = 2, lwd = 2)

### b) Encontre o EMV para d

#### derivada de n*log(d) -2*sum(log(y)) - (d+1)*sum(log(1+y^(-1))) em d = n/d - sum(log(1+y^(-1)))
#### igualando a zero: n/d - sum(log(1+y^(-1))) -> 1/d = sum(log(1+y^(-1)))/n -> d = n/sum(log(1+y^(-1)))

emv <- function(y){
    n <- length(y)
    n/sum(log(1+y^(-1)))
}

emv(y = c(3, 2, 5:7))

abline(v = emv(y = c(3, 2, 5:7)))

### c) Faça uma aproximação em série de Taylor até 2ª ordem para a função deviance

#### Aproximação Taylor 2ª ordem no ponto x0:
#### f(x) em x0 aprox. f(x0) + (x-x0)*f'(x0) + ((x-x0)^2/2)*f''(x0)
#### Dev(d) no ponto d0 aprox. -2*[l(d) - l(d0)] = 2*[l(d0) - l(d)] = 2*[l(d0) - (l(d0) + (d-d0)*l'(d) + ((d-d0)^2/2)*l''(d))]
#### Dev(d) = -2*n*(log(d) - log(d0)) -2*sum(log(1+y^(-1)))*(d0 - d)
#### Dev'(d) = -2*n*(d^(-1))+2*sum(log(1+y^(-1)))
#### Dev''(d) = 2*n*(d^(-2))
#### Dev(d) em d0 aprox. -(d-d0)^2*l''(d0) = -(d-d0)^2*(2n(d0^(-2))) = -2n(d-d0)^2/d0^2 = -2n(d^2 - 2dd0 + d0^2)/d0^2
#### como d0 = n/sum(log(1+y^(-1))): -2n(d-(n/sum(log(1+y^(-1)))))^2/(n/sum(log(1+y^(-1))))^2 = 2*sum(log(1+y^(-1)))^2(d-(n/sum(log(1+y^(-1)))))^2/n

Deva <- function(d, y){
    n <- length(y)
    d0 <- emv(y)
    (d-d0)^2*(n*(d0^(-2)))
}

curve(Deva(d, y = c(3, 2, 5:7)), from = 0.1, to = 10, xname = "d", add = TRUE, col = 4, lwd = 2, lty = 3)

##curve(Deva(d, y = c(3, 2, 5:7)), from = 1.5, to = 9.6, xname = "d", add = TRUE, col = 4, lwd = 2, lty = 3)
abline(h = 3.84, col = "grey")

### d) Encontre o IC baseado na aproximação obtido na letra c)
##d0 +- sqrt(c*/Dev''(d0))
##d0 +- sqrt(c*/2*n*(d0^(-2)))
##d0 +- sqrt(c*/2*n/(d0^2))
##d0 +- sqrt(c*(d0^2)/2n)

ic <- function(y, c = 3.84){
    n <- length(y)
    d0 <- emv(y)
    d0+c(-1, 1)*sqrt(c/(n/(d0^2)))
}

abline(v = ic(y = c(3, 2, 5:7)), col = 4)

### e) Considere que as observações são y_1 = 3, y_2 = 2, y_3 = 5, y_4 = 6 e y_5 = 7.
###    Forneça a estimativa de máxima verossimilhança com um intervalo de 95% de confiança conforme obtido em d).
y <- c(3, 2, 5:7)
emv(y)
ic(y)

### f) Usando a função deviance "exata" obtenha um IC usando algum algoritmo numérico.
###    Descreva os passos do algoritmo e use o computador para fazer as contas.

#### 1) Programar a função em que se deseja encontrar os pontos
Dev <- function(d, y = c(3, 2, 5:7)){
    n <- length(y)
    -2*n*(log(d) - log(n/sum(log(1+y^(-1))))) -2*sum(log(1+y^(-1)))*(n/sum(log(1+y^(-1))) - d)
}

pontoDev <- function(d, y = c(3, 2, 5:7), ponto = 3.84){
    dev <- Dev(d, y)
    dev-ponto
}

#### 2) Programar o método (no caso, o da bisseção)
bissecao <- function(fx, a, b, tol = 1e-04, max_iter = 100){
    fa <- fx(a);  fb <-fx(b)
    if(fa*fb > 0) stop("Solução não está no intervalo")
    solucao <- c()
    sol <- (a + b)/2
    solucao[1] <- sol
    limites <- matrix(NA, ncol = 2, nrow = max_iter)
    for(i in 1:max_iter){
        test <- fx(a)*fx(sol)
        if(test < 0){
            solucao[i+1] <- (a + sol)/2
            b = sol
        }
        if(test > 0){
            solucao[i+1] <- (b + sol)/2
            a = sol
        }
        if(abs(b-a)/2 < tol) break
        sol = solucao[i+1]
        limites[i, ] <- c(a, b)
    }
    out <- list("Tentativas" = solucao, "Limites" = limites, "Raiz" = solucao[i+1])
    return(out)
}

#### O método usa de confinamento é iniciado avaliando, as froteiras de um intervalo fornecido em que é testado se essas fronteiras apresentam sinais opostos. É calculado o próximo ponto pela média das fronteiras. Essa nova estimativa ocupa o lugar local da fronteira que tiver o mesmo sinal. O procedimento é repetido até que a tolerância seja satisfeita

pontoDev(0.1, y)

pontoDev(max(y), y)

liic <- bissecao(fx = pontoDev, a = 0.1, b = emv(y))$Raiz
lsic <- bissecao(fx = pontoDev, a = emv(y), b = 20)$Raiz

##curve(Dev(d, y = c(3, 2, 5:7)), from = 1.5, to = 9.6, xname = "d", add = TRUE, col = 2, lwd = 2, lty = 3)
abline(v = liic, col = 3)
abline(v = lsic, col = 3)

